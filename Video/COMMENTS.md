# Comments on the Search and Map Video

The submitted video includes footage from many sources of the search and map mission described in the paper. The full mission duration is 12 minutes, but we accelerated it to be shorter.

The webcam that was mounted onboard was very light but had very bad image definition for the light conditions present and we weren't able to correct this behaviour during the testing sessions. However, this webcam was only included for documentation purposes as the image captures for target processing were taken from the Pi Camera V2. We included this footage in the video, but we intend to change the webcam for future missions.

The manual pilot can be seen in many parts visually monitoring the UAV with the RC at hand ready to take manual control if any system failure is evident. This was useful during initial testing and calibration, but for this mission the manual pilot was only needed for takeoff and landing.

The following comments refer to the video file Search and Map.mp4 for the specified timestamp. 

* 0:00 - 0:03 -> The place where the mission took place is shown here, with the compound red target (two colors) and the GCS.
* 0:04 - 0:10 -> Manual takeoff. In the bottom right window waypoints that correspond to the first 300 cells are shown, as a consequence of the onboard computer sending the cells of the region to the GCS. The trajectory being displayed will be discarded when the mission starts. 
* 0:11 - 0:14 -> The pilot with the RC manually flies the UAV to the mission height of 150m and then engages the RTL mode to make the UAV cirlce near the takeoff location.
* 0:21 - 0:23 -> The discrete event controller is engaged by changing the flight mode to LOITER via the console (bottom center). Then the takeOff action sets waypoints for the UAV to perform one loop arriving at the current location with a direction parallel to the grid direction. After these waypoints are loaded the controller commands the UAV to enter its AUTO mode for waypoint following.
* 0:24 - 0:25 -> The UAV finishes the loop and flies to the cells as they are provided by the iterator, capturing and processing images on arrival.
* 0:26 - 0:28 -> Example of bad communication happening between the onboard computer and the GCS. In this case the UAV's location is updated with lag and the information that some cells where visited doesn't arrive to the GCS. This is why some flags at the top corner are not marked as visitied. Note that iterator on board knows that they have been visited. This is a display issue.
* 0:36 -> First target was found in an image captured on arrival at a cell (see file 35_rgb.png in the captures folder). It is marked on the map in red.
* 0:36 - 0:42 -> They UAV covers all non-visited locations that are adjacent to cells known to have the target. 
* 0:42 - 0:43 -> The rest of the cells are removed from the iterator as there are no more cells that answer affirmatively to has.target.next. After this is finished the land action is executed (RTL).
* 0:44 - 0:50 -> Top right of screen shows the discretized region of 29.502 cells. The UAV ends up flying only in a small top portion of the area.
* 0:51 - 0:59 -> Manual landing takes place with the pilot using the RC.
