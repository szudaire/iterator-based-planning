# Iterator Based Planning

This repository is separated into five folders:

* Captures real flight: Images captured by the Pi Camera V2 in-flight
* Footage: Some photos taken during the mission setup and preparation
* MTSA: Full specifications and MTSA tool for the controller synthesis
* Video: Another version of the video submitted to ICRA-2020 with a separate file with comments about the video.
* Extra real flight: Second real flight test of the same mission specification

## flight_path.png

This image shows the flight path for the UAV with the discrete controller engaged and the order in which cells where visited for the mission.

## Captures real flight

Here are the images that were taken in-flight and processed at run-time to answer the has.target.curr question. Image number corresponds with the order in which locations where visited during the mission. Affirmative answers (yes.target.curr) were given for images 35, 38, 39, 40, 42 and 43.

Target identification was based on a simple red filter implemented by the "_process_image(image)" method of the python script "planner_targetsensor.py". The HSV color range to detect red pixels was (136,45,130) to (197,210,240), only answering yes.target.curr if the amount of detected pixels was 8 or above.

## Footage

Here are some photos taken during mission setup and preparation. The red target can be seen on images IMG_00(28-31).JPG.

## MTSA & Models

In this folder you can find:

* mtsa.jar: MTSA tool written in Java for discrete controller synthesis. To correctly synthesise controllers from the .lts file specifications, the Java Virtual Machine must be started with a heap space of around 6.5 Gb. In linux you can use the following command:

```
java -Xmx6500m -jar mtsa.jar
```

* MTSA-userGuide.pdf: User guide for the MTSA tool. For synthesising the controller click on the Compile button after opening the file and selecting TestController from the dropdown menu.
* Nemo.lts: System model and specifications for the Nemo Search and Follow mission.
* Cover.lts: System model and specifications for the Cover mission.
* Ordered Patrol.lts: System model and specifications for the ordered patrol of 3 locations mission.
* Patrol Fire.lts: System model and specifications for the Fire Patrol mission.
* Search and Map.lts: System model and specifications for the Search and Map mission.
* Output controllers: In this folder you will find the MTSA generated .txt files for the synthesized controller for each spec.

## Extra real flight

In this folder you can find another real flight that took place the 16th of March 2019. The mission is the same as presented in paper submission for ICRA-2020 but with a smaller region (314 cells) and better video documentation (fixed an onboard webcam issue). The flight parameters were the same as the first real flight presented.


