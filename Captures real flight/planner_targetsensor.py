from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from picamera import PiCamera
from os.path import expanduser
import numpy as np

class TargetSensor(Module):
	def init(self):
		home = expanduser("~")
		self.path = home + '/.cache/mavproxy/captures/'

		self._process_type = 1
		return
	
	def _started(self):
                self.width_rgb = 640
                self.height_rgb = 480
                
                self.picam = PiCamera()
                self.picam.resolution = (self.width_rgb, self.height_rgb)
                self.picam.framerate = 24

                self._img_num = 0

                log = open(self.path + "log.txt","a+")
                log.write("Image number,position[0],position[1],altitude,roll,battery_per,battery_vol\n")
                log.close()
	
	def _capture(self):
                self._img_num += 1
                image = self._capture_rgb()
                self._write_to_log()

                self._process_image(image)
                cv.imwrite(self.path + str(self._img_num) + "_rgb.png",image)
		return

	def add_locations(self,pos_locations):
		self._location_list = Array('i',range(len(pos_locations)))
		self._array_end = Value('i',0)

	def _add_to_array_end(self):
                self._array_end.value += 1
                
	def _substract_to_array_end(self):
                self._array_end.value -= 1

        def _get_array_end(self):
                return self._array_end.value

        def _add_to_list(self,value):
                self._location_list[self._get_array_end()] = value
                self._add_to_array_end()

        def _get_list(self):
                return self._location_list[0:self._get_array_end()]

        def _remove_from_list(self,value):
                array = np.array(self._get_list())
                ind = np.flatnonzero(array==value)[0]
                aux = self._location_list[ind+1:self._get_array_end()]
                self._substract_to_array_end()
                self._location_list[ind:self._get_array_end()] = aux
                
        def _process_image(self,image):
                hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
                # define range of blue color in HSV
                lower_red = np.array([136,45,130])
                upper_red = np.array([197,210,240])
                mask = cv.inRange(hsv, lower_red, upper_red)
                
                #blur = cv.blur(mask,(5,5))

                current = self._sharedmem.get_current_location()
                if (len(mask[mask>200]) > 7):
                        self._field_of_view = 110.0/100.0 #100 meters at 100 meters of altitude
                        ground_width = self._field_of_view*self._sharedmem.get_flight_height()
                        area = self._sharedmem.get_discretizer().get_capture_polygon(current,ground_width)
                        elements = self._sharedmem.get_discretizer().get_locations_from_area(area,0)
                        for elem in elements:
                                if (elem not in self._get_list()):
                                        self._add_to_list(elem)
                        loc_current_pos = self._sharedmem.get_discretizer().get_position(current)
                        self._add_to_event_queue('yes_target_curr')

                        #Send found to ground station
                        self._add_to_message_queue(mesg.FOUND,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                        time.sleep(0.1)
                        self._add_to_message_queue(mesg.FOUND,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                else:
                        if (current in self._get_list()):
                                self._remove_from_list(current)
                        self._add_to_event_queue('no_target_curr')
                return

        def _write_to_log(self):
                #Image number,position[0],position[1],altitude,roll,battery_per,battery_vol
                roll = self._sharedmem.get_roll()
		altitude = self._sharedmem.get_relative_altitude()
		position = self._sharedmem.get_position()
		battery_per,battery_vol = self._sharedmem.get_battery()

                log = open(self.path + "log.txt","a+")
                log.write(str(self._img_num) + "," + str(position[0]) + "," + str(position[1]) + "," + str(altitude) + "," + str(roll) + "," + str(battery_per) + "," + str(battery_vol) + "\n")
                log.close()
	
	def has_target_curr(self):
		self._add_to_command_queue('capture')

	def has_target_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (next_pos in self._get_list()):
                        return ['yes_target_next']
                else:
                        return ['no_target_next']

        def _capture_rgb(self):
                t1 = time.time()
                
                image = np.empty((self.height_rgb * self.width_rgb * 3,), dtype=np.uint8)
                self.picam.capture(image,format='bgr',use_video_port=True)
                image = image.reshape((self.height_rgb, self.width_rgb, 3))
                
                t2 = time.time()
                self._add_to_message_queue(mesg.PRINT,"RGB in " + str(t2-t1) + " secs.")
                #cv.imshow('capture rgb',image)
                return image
