# Comments for "Full mission.mp4"

This video shows a second test flight of the same search and map mission described in the paper. For this test we fixed the webcam issue that was present in the submitted video.

The following comments refer to the video for the specified timestamp.

* 0:00 - 1:09 -> Manual takeoff
* 1:10 - 1:23 -> RTL mode engaged
* 1:24 -> Discrete Event Controller started
* 4:27 -> Mission End, the discrete controller engages the RTL mode
* 4:55 - 5:51 -> Manual landing

# Comments for "Mission configuration and start.mp4"

This video shows the initial setup that is required to start the mission. 

* 0:00 - 0:03 -> The mavproxy instance running our planning software is started on the Raspberry Pi.
* 0:05 - 0:13 -> The region to discretize is loaded from the text file "lj_v2.txt" as a polygon described by a set of 6 geo-fence points.
* 0:23 - 0:34 -> The mission flight parameters are loaded twice from the GCS to the onboard computer, just to make sure everything was set correctly. The Raspberry Pi replies with "SUCCESS" for the 4 parameters that were set.
* 0:35 - 1:00 -> The start command is executed which commands the onboard computer to discretize the region and to send the resulting cells to be displayed by the GCS (green flags on the screen).
* 1:20 - 1:26 -> Communication is tested between the GCS and the onboard computer with a pinging command.
* 1:30 - 1:43 -> The webcam on the UAV is commanded to start recording (only for documentation purposes).
* 4:08 -> UAV manual takeoff.

