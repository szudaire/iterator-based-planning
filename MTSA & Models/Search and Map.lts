set PlaneC = {takeoff, go_next, land,restart_mission}
set PlaneU = {initial_config, arrived, takeoff_ended, landed, no_battery}
set PlaneA = {PlaneC,PlaneU}

set IteratorC = {has_next,reset_iterator,remove_next}
set IteratorU = {yes_next, no_next}
set IteratorA = {IteratorC,IteratorU}

set SensorsC = {has_target_next,has_target_curr}
set SensorsU = {yes_target_next,no_target_next,yes_target_curr,no_target_curr}
set Sensors = {SensorsC,SensorsU}

Plane = (initial_config -> ConfigDone),
ConfigDone = (	takeoff -> takeoff_ended -> TakeoffEnded),
TakeoffEnded = (	{IteratorC,SensorsC} -> TakeoffEnded | 
					go_next -> GoTo |
					land -> landed -> Landed),
Landed = (restart_mission -> Plane),
GoTo = (arrived -> TakeoffEnded).

Iterator = (has_next -> (yes_next -> YesNext | no_next -> NoNext)),
YesNext = (remove_next -> Iterator),
NoNext = (reset_iterator -> Iterator).

GoNext = (yes_next -> (go_next -> ({remove_next} -> GoNext) 
		| {remove_next} -> GoNext)).

CurrentQuestions = (has_next -> CurrentQuestions | arrived -> CQNext),
CQNext = (has_next -> CurrentQuestions | has_target_curr -> CQNext).

NextQuestions = ({remove_next} -> NextQuestions | yes_next -> NQYes),
NQYes = ({remove_next} -> NextQuestions | {has_target_next} -> NQYes).

Battery = (takeoff_ended -> InFlightBat | takeoff_ended -> InFlightNoBat),
InFlightBat = (land -> Battery),
InFlightNoBat = (land -> Battery | 
				no_battery -> land -> Battery).

TargetSensor = (has_target_next -> (yes_target_next -> TargetSensor | no_target_next -> TargetSensor)
			| has_target_curr -> (yes_target_curr -> TargetSensor | no_target_curr -> TargetSensor))
+{no_battery}.

set Alphabet = {IteratorA,PlaneA,Sensors}
set Controlables = {IteratorC,PlaneC,SensorsC}
||Environment = (Plane || Iterator || CurrentQuestions || NextQuestions || GoNext || TargetSensor).// || Battery).

////////////False and True
fluent True = <Alphabet,Alphabet\Alphabet> initially 1
fluent False = <Alphabet\Alphabet,Alphabet> initially 0

////////////LIVENESS
fluent F_has_next = <has_next,Alphabet\{has_next}> initially 0
assert A_HasNext = (F_has_next)

////////////FAILURE
set FailActions = {no_battery}
fluent F_FAILURES = <FailActions,Alphabet\{FailActions}> initially 0
assert Failures = (F_FAILURES)

////////////SAFETY
fluent F_NoBattery = <no_battery,landed> initially 0
fluent F_NoNext = <no_next,has_next> initially 0
fluent F_FoundTarget = <yes_target_curr,reset_iterator> initially 0
fluent F_FoundTargetHold = <yes_target_curr,land> initially 0

ltl_property L_LandOnNoBatteryOrFinished = ([](no_battery -> (!{IteratorC} W land))
														&& [](no_next && !F_FoundTarget -> (!{IteratorC} W land)) 
														&& [](land -> (F_NoBattery || (F_NoNext && !F_FoundTarget))))

fluent F_YesTargetCurrent = <yes_target_curr,has_next> initially 0
fluent F_NoTargetCurrent = <no_target_curr,has_next> initially 0
fluent F_YesTargetNext = <yes_target_next,has_next> initially 0
fluent F_NoTargetNext = <no_target_next,has_next> initially 0
fluent F_Gone = <go_next, has_next> initially 0
assert Mapping = (F_FoundTargetHold)
assert Searching = (!F_FoundTargetHold)

assert RespondedTargetNext = (F_YesTargetNext || F_NoTargetNext)
assert RespondedTargetCurrent = (F_YesTargetCurrent || F_NoTargetCurrent)
assert DoneSearching = (RespondedTargetCurrent)
assert GoneConditionSearching = (F_Gone)
assert Achieve_ArriveConditionSearching = (!{IteratorC} W DoneSearching)
assert Achieve_GoneConditionSearching = (!{IteratorC} W GoneConditionSearching)
ltl_property L_ArrivedConditionSearching = [](arrived && Searching -> Achieve_ArriveConditionSearching)
ltl_property L_GoneConditionSearching = [](yes_next && Searching -> Achieve_GoneConditionSearching)

assert DoneMapping = (RespondedTargetCurrent)
assert GoneConditionMapping = ((F_YesTargetNext <-> F_Gone) && RespondedTargetNext)
assert Achieve_ArriveConditionMapping = (!{IteratorC} W DoneMapping)
assert Achieve_GoneConditionMapping = (!{IteratorC} W GoneConditionMapping)
ltl_property L_ArrivedConditionMapping = [](arrived && Mapping -> Achieve_ArriveConditionMapping)
ltl_property L_GoneConditionMapping = [](yes_next && Mapping -> Achieve_GoneConditionMapping)

controllerSpec Controller_Req = {
        failure = {Failures}
        safety = {L_LandOnNoBatteryOrFinished,
					L_ArrivedConditionSearching,L_GoneConditionSearching,
					L_ArrivedConditionMapping,L_GoneConditionMapping,
					}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||Controller = (Environment)~{Controller_Req}.
minimal ||TestController = (Controller || Environment).
